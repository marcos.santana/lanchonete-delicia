package com.example.android.lanchonetedelicia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetalheActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);

        Intent intent = DetalheActivity.this.getIntent();
        Produto produto = (Produto) ((Intent) intent).getSerializableExtra("produto");

        TextView textViewNome = (TextView) findViewById(R.id.produtoNome);
        textViewNome.setText(produto.getNome());
        TextView textViewValor = (TextView) findViewById(R.id.valorProduto);
        textViewValor.setText(produto.getPreco());
        TextView  textViewDescricao = (TextView) findViewById(R.id.descricaoProduto);
        textViewDescricao.setText(produto.getDescricao());

        ImageView imageView = (ImageView) findViewById(R.id.imagemProduto);
        Picasso.get().load(produto.getImagem()).into(imageView);

    }
}
