package com.example.android.lanchonetedelicia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Produto> produtos;
    private ArrayAdapter<Produto> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        produtos = new ArrayList<>();
        adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, produtos);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        AsyncHttpClient client = new AsyncHttpClient();

        client.get("https://patra-backend.appspot.com/produtos", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(MainActivity.this, "Falha", Toast.LENGTH_LONG);
                Log.d("AsyncHTTPClient", "response" +  responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.d("AsyncHTTPClient", "response" + responseString);
                Gson gson = new GsonBuilder().create();
                Produto[] produtos = gson.fromJson(responseString, Produto[].class);
                adapter.clear();
                for(Produto produto: produtos){
                    adapter.add(produto);
                }
            }
        });

        ListView lista = (ListView)findViewById(R.id.produtos);
        lista.setAdapter(adapter);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView adapterView, View view, int posicao, long l ) {
                Intent detalheIntent = new Intent(MainActivity.this, DetalheActivity.class);
                detalheIntent.putExtra("produto", produtos.get(posicao));
                startActivity(detalheIntent);
            }
        });

    }
}
