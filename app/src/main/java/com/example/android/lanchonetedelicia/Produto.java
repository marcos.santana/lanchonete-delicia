package com.example.android.lanchonetedelicia;

import java.io.Serializable;

public class Produto implements Serializable {
//    {"_id":1,"nome":"Produto 01","preco":"10,00","descricao":"Descricao","imagem":"https://patra-backend.appspot.com/imagem01.png"
    private int id;
    private String nome;
    private String preco;
    private String descricao;
    private String imagem;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    @Override
    public String toString() {
        return  nome;
    }
}
